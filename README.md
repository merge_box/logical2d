# Logical2d
Really pre-programming phase.

### Build

```sh
mkdir builds~
cd ./builds~
cmake ..
make
```

### Compile
To compile:

```sh
g++ init.cpp -o logical2d.so -Wall -I/usr/include/lua5.4 -llua5.4
```
