# Documentation

This is a documentation of logical2d. Features which are planed but not aviable are declared with `(cif)` which is short for `comming in future`. `(WiP)` is short for `(Work in Progress)` and means which can already used but isn't final.

# Table of contents
- [How the Engine works](engine.md)
- [Areas](areas.md)