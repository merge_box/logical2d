# How the Engine works

logical2d is splitted in 3 parts: `logical part`, `existing part` and `misc part`.

## Logical Part(cif)

In past was only this part planed thatcause the engine has them name. Other then other engines, logical2d use the `logical part`  for all what should drawn on the screen. Means if you register a rectangle it won't drawn on window. It's a logical object which isn't existing on a display, it's only theoreticaly. You can also use only logical part and use another engine to draw objects on display.

### Compile only logical part(cif)

## Existing Part(cif)

The `existing part` is an interface to `SDL` to paint things on the screen. It is only there to simplify the handling from logical to existing objects. When you understand more with logical2d you understand it more.

## Misc Part(cif)

This is only here that you don't need other libraries install with it. It add usefull things like a function that allows you to check which os you are using

### Deactivate all libraries of the `Misc Part`

### Deactivate only some libraries of the `Misc Part`

### Activate only some libraries of the `Misc Part`