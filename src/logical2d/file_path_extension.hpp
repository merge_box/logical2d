#ifndef LOGICAL2D_FILE_PATH_EXTENSION_HPP
#define LOGICAL2D_FILE_PATH_EXTENSION_HPP

#include <lua.hpp>

namespace logical2d {
	int file_path_extension(lua_State*);
}

#endif
