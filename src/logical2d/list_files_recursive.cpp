#include "list_files_recursive.hpp"

#include <lua.hpp>

#include <cstddef>
#include <filesystem>
#include <new>

namespace logical2d {
	int list_files_recursive(lua_State* L) {
		try {
			std::size_t path_length;
			const char* path_string = lua_tolstring(L, -1, &path_length);
			if(path_string == nullptr) { goto error; }

			lua_newtable(L);

			std::size_t i = 0;
			for(const std::filesystem::directory_entry& entry : std::filesystem::recursive_directory_iterator(std::filesystem::path(path_string, path_string + path_length))) {
				lua_pushinteger(L, ++i);
				lua_pushstring(L, entry.path().c_str());
				lua_settable(L, 2);
			}

			return 1;
		} catch(std::bad_alloc) {
			// Out of memory
		} catch(std::filesystem::filesystem_error) {
			// Error with filesystem, e.g. no permission or not a directory
		} catch(...) {
			// Shouldn't happen
		}
error:
		lua_pushnil(L);
		return 1;
	}
}
