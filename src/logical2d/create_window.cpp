#include <lua.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include "create_window.hpp"

namespace logical2d {
	int create_window(lua_State* L) {
		char window_name[64] = "logical2d";
		if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
			printf("error initializing SDL: %s\n", SDL_GetError());
		}
		SDL_Window* win = SDL_CreateWindow(window_name,
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			800, 600, 0);
		return 0;
		//return commonPush(L, "p", window_name, win);
	}
}
