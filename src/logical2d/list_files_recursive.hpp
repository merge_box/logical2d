#ifndef LOGICAL2D_LIST_FILES_RECURSIVE_HPP
#define LOGICAL2D_LIST_FILES_RECURSIVE_HPP

#include <lua.hpp>

namespace logical2d {
	int list_files_recursive(lua_State*);
}

#endif
