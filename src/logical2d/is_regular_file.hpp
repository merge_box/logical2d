#ifndef LOGICAL2D_IS_REGULAR_FILE_HPP
#define LOGICAL2D_IS_REGULAR_FILE_HPP

#include <lua.hpp>

namespace logical2d {
	int is_regular_file(lua_State*);
}

#endif
