#include "file_path_extension.hpp"

#include <lua.hpp>

#include <cstddef>
#include <filesystem>

namespace logical2d {
	int file_path_extension(lua_State* L) {
		std::size_t path_length;
		const char* path_string = lua_tolstring(L, 1, &path_length);
		if(path_string == nullptr) { goto error; }
		try {
			auto extension = std::filesystem::path(path_string, path_string + path_length).extension();
			lua_pushstring(L, extension.c_str());
			return 1;
		} catch(...) {
			// Unknown error
		}
error:
		lua_pushnil(L);
		return 1;
	}
}
