#include <lua.hpp>

#include "create_window.hpp"
#include "file_exists.hpp"
#include "file_path_extension.hpp"
#include "is_directory.hpp"
#include "is_regular_file.hpp"
#include "list_files.hpp"
#include "list_files_recursive.hpp"

extern "C" {
	int luaopen_logical2d(lua_State* L) {
		using namespace logical2d;

		static luaL_Reg functions[] = {
			{"create_window", create_window},
			{"file_exists", file_exists},
			{"file_path_extension", file_path_extension},
			{"is_directory", is_directory},
			{"is_regular_file", is_regular_file},
			{"list_files", list_files},
			{"list_files_recursive", list_files_recursive},
			{nullptr, nullptr},
		};
		luaL_newlib(L, functions);
		return 1;
	}
}
