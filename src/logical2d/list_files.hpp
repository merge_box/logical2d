#ifndef LOGICAL2D_LIST_FILES_HPP
#define LOGICAL2D_LIST_FILES_HPP

#include <lua.hpp>

namespace logical2d {
	int list_files(lua_State*);
}

#endif
