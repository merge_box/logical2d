#ifndef LOGICAL2D_CREATE_WINDOW_HPP
#define LOGICAL2D_CREATE_WINDOW_HPP

#include <lua.hpp>

namespace logical2d {
	int create_window(lua_State*);
}

#endif
