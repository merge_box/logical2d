#ifndef LOGICAL2D_FILE_EXISTS_HPP
#define LOGICAL2D_FILE_EXISTS_HPP

#include <lua.hpp>

namespace logical2d {
	int file_exists(lua_State*);
}

#endif
