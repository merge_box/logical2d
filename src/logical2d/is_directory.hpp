#ifndef LOGICAL2D_IS_DIRECTORY_HPP
#define LOGICAL2D_IS_DIRECTORY_HPP

#include <lua.hpp>

namespace logical2d {
	int is_directory(lua_State*);
}

#endif
