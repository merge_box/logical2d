Warning this i write only now that i not forgot the required programs. How it work exactly i must explain another time.

# Install this engine

## Prerequirements

### Linux

`sudo apt install lua5.4 liblua5.4-dev gpp`


## Build the engine

### Linux
```sh
mkdir builds~
cd ./builds~
cmake ..
make
```
After it there will be a executeable file called `logical2d.so`

#### CMake options

Use it like this:

```sh
cmake <path> <option1> <option2> <optionx>
```

You have following options:

`-DLUA_VERSION=<version>`

Specify which version you want use instead of `<version>`.
